# PurpleNetwork API Service Bundle

## About

This bundle helps to build API applications.

Step 1: Download the Bundle
---------------------------

Require the `purplenetwork/api-service-bundle` package in your composer.json and update your dependencies.

    $ composer require purplenetwork/api-service-bundle

Add the PNAPIServiceBundle to your application's kernel:

    public function registerBundles()
    {
        $bundles = array(
            ...
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new FOS\RestBundle\FOSRestBundle(),
            new PN\APIServiceBundle\PNAPIServiceBundle(),
            ...
        );
        ...
    }

## Configuration
