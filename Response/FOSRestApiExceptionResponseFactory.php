<?php

namespace PN\APIServiceBundle\Response;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FOSRestApiExceptionResponseFactory implements FOSRestApiExceptionResponseInterface
{
    /** @var  FOSRestApiResponseHelperService */
    protected $apiResponseHelper;

    public function __construct(FOSRestApiResponseHelperService $apiResponseHelper)
    {
        $this->apiResponseHelper = $apiResponseHelper;
    }

    /**
     * @param \Exception $exception
     * @return mixed
     */
    public function getResponse(\Exception $exception)
    {
        if ($exception instanceof AccessDeniedHttpException) {
            return $this->apiResponseHelper->createErrorViewForAccessDenied();
        }

        if ($exception instanceof NotFoundHttpException) {
            return $this->apiResponseHelper->createErrorViewForRouteOrResourceNotFound();
        }

        if ($exception instanceof MethodNotAllowedHttpException) {
            return $this->apiResponseHelper->createErrorViewForMethodNotAllowed();
        }

        return $this->apiResponseHelper->createGenericErrorView();
    }
}