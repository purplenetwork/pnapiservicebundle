<?php

namespace PN\APIServiceBundle\Response;

use AppBundle\Entity\User;
use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandler;
use PN\APIServiceBundle\Services\SerializerService;
use Symfony\Component\HttpFoundation\Response;

class FOSRestApiResponseHelperService
{
    protected $viewHandler;
    protected $serializerService;

    public function __construct(ViewHandler $viewHandler, SerializerService $serializerService)
    {
        $this->viewHandler          = $viewHandler;
        $this->serializerService    = $serializerService;
    }

    /**
     * Return a FOSRest response
     *
     * @param array $data
     * @param null $statusCode
     * @param array $headers
     * @return mixed
     */
    public function createHandledViewResponse($data = array(), $statusCode = null, array $headers = array())
    {
        return $this->viewHandler->handle(View::create($data, $statusCode, $headers));
    }

    /**
     * Return a view for a created resource
     *
     * @return View
     */
    public function createViewForResourceCreated(array $data = array())
    {
        return $this->createHandledViewResponse($data, Response::HTTP_CREATED);
    }

    /**
     * Return an error view for error "BAD REQUEST"
     *
     * @param $message string|array
     * @return View
     */
    public function createErrorViewForBadRequest($message)
    {
        return $this->createHandledViewResponse(array('error' => $message), Response::HTTP_BAD_REQUEST);
    }

    /**
     * Return an error view for error "UNAUTHORIZED"
     *
     * @param string $message
     * @return View
     */
    public function createErrorViewForUnauthorized($message)
    {
        return $this->createHandledViewResponse(array('error' => !empty($message) ? $message : 'You are not authorized'), Response::HTTP_UNAUTHORIZED);
    }


    /**
     * Return an error view for error "ACCESS DENIED"
     *
     * @return View
     */
    public function createErrorViewForAccessDenied()
    {
        return $this->createHandledViewResponse(array('error' => 'You are not authorized'), Response::HTTP_FORBIDDEN);
    }

    /**
     * Return an error view for error "ROUTE NOT FOUND"
     *
     * @param string $message
     * @return View
     */
    public function createErrorViewForRouteOrResourceNotFound($message = '')
    {
        return $this->createHandledViewResponse(array('error' => !empty($message) ? $message : 'Route or Resource not found'), Response::HTTP_NOT_FOUND);
    }

    /**
     * Return an error view for error "METHOD NOT ALLOWED"
     *
     * @return View
     */
    public function createErrorViewForMethodNotAllowed()
    {
        return $this->createHandledViewResponse(array('error' => 'Method not allowed.'), Response::HTTP_METHOD_NOT_ALLOWED);
    }

    /**
     * Return an error view for error "SERVER ERROR"
     *
     * @return View
     */
    public function createGenericErrorView()
    {
        return $this->createHandledViewResponse(array('error' => 'Ops. Something goes wrong!'), Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * If $user is a User object, add the serialized version to $response
     *
     * @param $response
     * @param $user
     * @return mixed
     */
    public function addSerializedUserToResponse($user, Response $response)
    {
        if (!$user instanceof User) {
            return $response;
        }

        return $this->addValueToResponseContent(
            $response,
            'user',
            $this->serializerService->serialize($user)
        );
    }

    /**
     * Get the deserialized array of a JSON encoded Response
     *
     * @param Response $response
     * @return mixed
     */
    private function getDeserializedResponseContent(Response $response)
    {
        return json_decode(trim($response->getContent()), true);
    }

    /**
     * Return true if a JSON encoded Response has $key
     *
     * @param $key
     * @return bool
     */
    private function responseContentHasKey(Response $response, $key)
    {
        $jsonDecodedContent = $this->getDeserializedResponseContent($response);

        return isset($jsonDecodedContent[$key]);
    }

    /**
     * Get a value of a JSON encoded Response
     *
     * @param $key
     * @return bool
     */
    public function getResponseContentValue(Response $response, $key)
    {
        $jsonDecodedContent = $this->getDeserializedResponseContent($response);

        return (isset($jsonDecodedContent[$key])) ? $jsonDecodedContent[$key] : null;
    }

    /**
     * Add a value to the content of a JSON encoded Response
     *
     * @param $response
     * @param $key
     * @param $value
     * @return mixed
     */
    private function addValueToResponseContent(Response $response, $key, $value)
    {
        $jsonDecodedContent = $this->getDeserializedResponseContent($response);

        $jsonDecodedContent[$key] = $value;

        return $response->setContent(json_encode($jsonDecodedContent));
    }
}