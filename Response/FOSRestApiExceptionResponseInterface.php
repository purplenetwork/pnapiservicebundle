<?php

namespace PN\APIServiceBundle\Response;

interface FOSRestApiExceptionResponseInterface
{
    /**
     * @param \Exception $exception
     * @return mixed
     */
    public function getResponse(\Exception $exception);
}