<?php

namespace PN\APIServiceBundle\Services;

use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;

class SerializerService
{
    protected $jmsSerializer;

    public function __construct(Serializer $jmsSerializer)
    {
        $this->jmsSerializer = $jmsSerializer;
    }

    /**
     * Serializza un oggetto con JMSSerializer
     *
     * @param $object
     * @param array $groups
     * @return mixed|string
     */
    public function serialize($object, array $groups = array('api'))
    {
        $context = SerializationContext::create()
            ->setSerializeNull(true)
            ->enableMaxDepthChecks();

        if (is_array($groups)) {
            $context->setGroups($groups);
        }

        return json_decode(
            $this->jmsSerializer->serialize($object, 'json', $context),
            true
        );
    }
}