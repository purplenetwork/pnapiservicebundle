<?php

namespace PN\APIServiceBundle\Services;

class CurlRequestService
{

    public function doPostRequest($url, $data)
    {
        $parsedUrl = parse_url($url);
        if (!$parsedUrl) {
            throw new \InvalidArgumentException('The url provided is not well formed');
        }

        $options = array(
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_MAXREDIRS => 5,     // Disabled SSL Cert checks
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data
        );

        if ($parsedUrl['scheme'] == 'https') {
            $options[CURLOPT_SSL_VERIFYPEER] = false;
        }

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);

        $result = array();
        $result['result'] = curl_exec($ch);
        $result['errorNumber'] = curl_errno($ch);
        $result['error'] = curl_error($ch);
        
        curl_close($ch);
        return $result;
    }

}