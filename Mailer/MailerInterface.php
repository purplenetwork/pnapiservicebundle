<?php

namespace PN\APIServiceBundle\Mailer;

/**
 * MailerService
 * 
 * @author Alfredo "stuzzo" Aiello <stuzzo@gmail.com>
 *
 */
interface MailerInterface
{
    /**
     * @param string $subject Email subject
     * @param string|array $fromEmail Email sender
     * @param string|array $toEmail Email recipient
     * @param string $templateHtml Email file template
     * @param array $parameters This array is used to put data inside email template
     * @return boolean
     */
    public function sendHtmlEmail($subject, $fromEmail, $toEmail, $templateHtml, $parameters = array());

    /**
     * @param string $subject Email subject
     * @param string|array $fromEmail Email sender
     * @param string|array $toEmail Email recipient
     * @param string $templateTextPlain Email string template
     * @return boolean
     */
    public function sendTextPlainEmail($subject, $fromEmail, $toEmail, $templateTextPlain);

    /**
     * @param string $subject Email subject
     * @param string|array $fromEmail Email sender
     * @param string|array $toEmail Email recipient
     * @param string $templateHtml Email file template
     * @param array $parameters This array is used to put data inside email template
     * @param string $templateTextPlain Email string template
     * @return boolean
     */
    public function sendHtmlAndTextPlainEmail($subject, $fromEmail, $toEmail, $templateHtml, $parameters = array(), $templateTextPlain);
}
