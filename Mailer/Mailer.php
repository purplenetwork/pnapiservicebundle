<?php

namespace PN\APIServiceBundle\Mailer;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * MailerService
 * 
 * @author Alfredo "stuzzo" Aiello <stuzzo@gmail.com>
 *
 */
class Mailer implements MailerInterface
{
    /** @var \Swift_Mailer  */
    protected $mailer;

    /** @var UrlGeneratorInterface  */
    protected $router;

    /** @var  EngineInterface */
    protected $templateEngine;

    /** @var  LoggerInterface */
    protected $logger;

    public function __construct(\Swift_Mailer $mailer, UrlGeneratorInterface $router, EngineInterface $templateEngine, LoggerInterface $logger)
    {
        $this->mailer = $mailer;
        $this->router = $router;
        $this->templateEngine = $templateEngine;
        $this->logger = $logger;
    }

    public function sendHtmlEmail($subject, $fromEmail, $toEmail, $templateHtml, $parameters = array())
    {
        $rendered = $this->templateEngine->render($templateHtml, $parameters);
        return $this->sendEmailMessage($subject, $fromEmail, $toEmail, $rendered);
    }

    public function sendTextPlainEmail($subject, $fromEmail, $toEmail, $templateTextPlain)
    {
        return $this->sendEmailMessage($subject, $fromEmail, $toEmail, '', false, $templateTextPlain, true);
    }

    public function sendHtmlAndTextPlainEmail($subject, $fromEmail, $toEmail, $templateHtml, $parameters = array(), $templateTextPlain)
    {
        $rendered = $this->templateEngine->render($templateHtml, $parameters);
        return $this->sendEmailMessage($subject, $fromEmail, $toEmail, $rendered, true, $templateTextPlain, true);
    }

    protected function sendEmailMessage($subject, $fromEmail, $toEmail, $templateHtml = '', $isHtml = true, $templateTextPlain = '', $isTextPlain = false)
    {
        /** @var \Swift_Message $message */
        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($fromEmail)
            ->setTo($toEmail);

        if ($isHtml) {
            $message->addPart($templateHtml, 'text/html');
        }

        if ($isTextPlain) {
            $message
                ->setContentType("text/plain; charset=UTF-8")
                ->setBody($templateTextPlain, 'text/plain');
        }

        $failures = null;
        $sent = $this->mailer->send($message, $failures);
        if (!$sent) {
            $this->logger->error(is_array($failures) ? json_decode($failures) : $failures);
        }
        return $sent;
    }
}
