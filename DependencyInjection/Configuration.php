<?php

namespace PN\APIServiceBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * 
 * @author Alfredo "stuzzo" Aiello <stuzzo@gmail.com>
 * 
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface {

    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder() {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('pn_service_api');
        $rootNode
                ->children()
                    ->arrayNode("response")
                        ->addDefaultsIfNotSet()
                        ->children()
                            ->scalarNode('allowed_domain')->defaultValue('*')->end()
                        ->end()
                    ->end()
                ->end();
        return $treeBuilder;
    }

}
