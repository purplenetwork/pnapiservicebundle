<?php

namespace PN\APIServiceBundle\Logger;

use Psr\Log\LoggerInterface;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Request;

class LogExceptions
{
    /** @var LoggerInterface */
    protected $logger;

    public function __construct(LoggerInterface $loggerInterface)
    {
        $this->logger = $loggerInterface;
    }

    public function log(\Exception $exception, Request $request = null)
    {
        $this->logText('**********************Start Exception***************************', $exception);
        $this->logText('Message', $exception);
        $this->logText($exception->getMessage(), $exception, 2);
        if (!empty($request)) {
            $this->logText('Request', $exception);
            $this->logKeyValue('Method', $request->getMethod(), $exception);
            $this->logKeyValue('Url', $request->getRequestUri(), $exception);
            $this->logText('Headers', $exception, 2);
            $this->logCollection($request->headers->all(), $exception);
            if ($request->getMethod() === 'POST') {
                $inputData = $request->request->all();
            } else {
                $inputData = $request->query->all();
            }
            $this->logText('Data', $exception, 2);
            $this->logCollection($inputData, $exception);
            $files = $request->files->all();
            if (count($files)) {
                $this->logText('File', $exception, 2);
                $this->logCollection($request->files->all(), $exception);
            }
        }

        $this->logText('Stack trace', $exception);
        foreach ($exception->getTrace() as $trace) {
            if (!empty($trace['file'])) {
                $traceMessage = sprintf('at %s line %s', $trace['file'], $trace['line']);
                $this->logText($traceMessage, $exception, 2);
            }
        }
        $this->logText('**********************End Exception***************************', $exception);
    }

    private function logText($value, \Exception $exception, $numberSpaces = 0)
    {
        $this->logger->error(str_repeat(' ', $numberSpaces) . $value, array('exception' => $exception));
    }

    private function logKeyValue($key, $value, \Exception $exception, $numberSpaces = 2)
    {
        $this->logger->error(str_repeat(' ', $numberSpaces) . $key . ': ' . $value, array('exception' => $exception));
    }

    private function logCollection($data, \Exception $exception, $numberSpaces = 4)
    {
        foreach ($data as $key => $value) {
            $this->logKeyValue($key, is_array($value) ? json_encode($value) : $value, $exception, $numberSpaces);
        }
    }

}