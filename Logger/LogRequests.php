<?php

namespace PN\APIServiceBundle\Logger;

use Psr\Log\LoggerInterface;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Request;

class LogRequests
{
    /** @var LoggerInterface */
    protected $logger;

    public function __construct(LoggerInterface $loggerInterface)
    {
        $this->logger = $loggerInterface;
    }

    /**
     * @param Request $request
     * @param string $message
     */
    public function log(Request $request, $message)
    {
        $this->logText('**********************Start Request***************************');
        $this->logText('Message');
        $this->logText($message, 2);
        $this->logText('Request');
        $this->logKeyValue('Method', $request->getMethod());
        $this->logKeyValue('Url', $request->getRequestUri());
        $this->logText('Headers', 2);
        $this->logCollection($request->headers->all());
        if ($request->getMethod() === 'POST') {
            $inputData = $request->request->all();
        } else {
            $inputData = $request->query->all();
        }
        $this->logText('Data', 2);
        $this->logCollection($inputData);
        $files = $request->files->all();
        if (count($files)) {
            $this->logText('File', 2);
            $this->logCollection($request->files->all());
        }
        $this->logText('**********************End Request***************************');
    }

    private function logText($value, $numberSpaces = 0)
    {
        $this->logger->error(str_repeat(' ', $numberSpaces) . $value);
    }

    private function logKeyValue($key, $value, $numberSpaces = 2)
    {
        $this->logger->error(str_repeat(' ', $numberSpaces) . $key . ': ' . $value);
    }

    private function logCollection($data, $numberSpaces = 4)
    {
        foreach ($data as $key => $value) {
            $this->logKeyValue($key, is_array($value) ? json_encode($value) : $value, $numberSpaces);
        }
    }

}